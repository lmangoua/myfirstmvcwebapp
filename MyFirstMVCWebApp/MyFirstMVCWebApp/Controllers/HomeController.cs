﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstMVCWebApp.Controllers
{

    public class HomeController : Controller
    {

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(string id)
        {
            if (id == null)
                return View();
            return View("Contact");
        }

        public ActionResult Contact()
        {
            return View();
        }


    }
}