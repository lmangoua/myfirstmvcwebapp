﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFirstMVCWebApp.Controllers
{
    public class HelloController : Controller
    {
        // GET: Hello
        public string SayHello(string fname, string lname)
        {
            return $"Hello {fname} {lname}";
        }
    }
}